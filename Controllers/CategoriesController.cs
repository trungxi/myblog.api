﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using myblog.api.Dtos;
using myblog.api.Dtos.Category;
using myblog.api.Entities;
using myblog.api.Services;

namespace myblog.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriesController : ControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoriesController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var result = await _categoryService.GetAllAsync();
            return Ok(result);
        }

        [HttpGet("postCount")]
        public async Task<IActionResult> GetAllWithPostCount()
        {
            var result = await _categoryService.GetAllWithPostCountAsync();
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await _categoryService.GetByIdAsync(id);
            return Ok(result);
        }

        [HttpPost]
        [Authorize]
        public async Task<IActionResult> Create([FromBody]CreateCategoryRequest request)
        {
            await _categoryService.CreateAsync(request);
            return Ok();
        }

        [HttpPut]
        [Authorize]
        public async Task<IActionResult> Update([FromBody]UpdateCategoryRequest request)
        {
            await _categoryService.UpdateAsync(request);
            return Ok();
        }

        [HttpDelete]
        [Authorize]
        public async Task<IActionResult> Delete([FromBody]BaseDto request)
        {
            await _categoryService.DeleteAsync(request);
            return Ok();
        }
    }
}