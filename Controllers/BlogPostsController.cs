﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using myblog.api.Dtos;
using myblog.api.Dtos.BlogPost;
using myblog.api.Services;

namespace myblog.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BlogPostsController : ControllerBase
    {
        private readonly IBlogPostService blogPostService;
        public BlogPostsController(IBlogPostService blogPostService)
        {
            this.blogPostService = blogPostService;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetById(int id)
        {
            var result = await blogPostService.GetByIdAsync(id);
            return Ok(result);
        }

        [HttpGet("{slug}")]
        public async Task<IActionResult> GetBySlug(string slug)
        {
            var result = await blogPostService.GetBySlug(slug);
            return Ok(result);
        }

        [HttpGet]
        public async Task<IActionResult> GetByCriteria([FromQuery]GetPostsByCriteriaRequest request)
        {
            var result = await blogPostService.GetPostsByCriteriaAsync(request);
            return Ok(result);
        }


        [HttpGet("bycategory/{categoryId}")]
        public async Task<IActionResult> GetByCategory(int categoryId, [FromQuery]GetPostByCategoryRequest request)
        {
            request.CategoryId = categoryId;
            var result = await blogPostService.GetByCategoryAsync(request);
            return Ok(result);
        }


        [HttpGet("search")]
        public async Task<IActionResult> Search([FromQuery]SearchPostRequest request)
        {
            var result = await blogPostService.SearchPostsAsync(request);
            return Ok(result);
        }
        
        [HttpPost]
        //[Authorize]
        public async Task<IActionResult> Create([FromBody]CreateBlogPostRequest request)
        {
            await blogPostService.CreateAsync(request);
            return Ok();
        }

        [HttpPut]
        //[Authorize]
        public async Task<IActionResult> Update([FromBody]UpdateBlogPostRequest request)
        {
            await blogPostService.UpdateAsync(request);
            return Ok();
        }

        [HttpDelete]
        //[Authorize]
        public async Task<IActionResult> Delete([FromBody]BaseDto request)
        {
            await blogPostService.DeleteAsync(request);
            return Ok();
        }
    }
}
