﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace myblog.api.Entities
{
    [Table("BlogPost")]
    public class BlogPost : BaseEntity
    {
        public string Title { get; set; }

        public string Slug { get; set; }

        public string Introduction { get; set; }

        public string BodyContent { get; set; }

        public string Thumbnail { get; set; }

        public string AuthorName { get; set; }

        public DateTime PublishDate { get; set; }

        public int CategoryId { get; set; }

        public int CommentCount { get; set; }

        public int LikeCount { get; set; }

        [ForeignKey("CategoryId")]
        public virtual Category Category { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }
    }
}
