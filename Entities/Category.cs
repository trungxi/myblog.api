﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace myblog.api.Entities
{
    [Table("Category")]
    public class Category : BaseEntity
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public virtual ICollection<BlogPost> BlogPosts { get; set; }
    }
}
