﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Entities
{
    public class Comment : BaseEntity
    {
        public int PostId { get; set; }

        public string Content { get; set; }

        public string Author { get; set; }

        public string AuthorEmail { get; set; }

        public int LikeCount { get; set; }

        [ForeignKey("CategoryId")]
        public virtual BlogPost BlogPost { get; set; }
    }
}
