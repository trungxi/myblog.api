﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using myblog.api.Infrastructure.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace myblog.api.Infrastructure.Middlewares
{
    public class ExceptionHandlerMiddleware
    {
        private readonly ILogger _logger;
        private readonly RequestDelegate _next;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                _logger.LogDebug("Request pipeline started.");
                await _next(httpContext);
            }
            catch (Exception ex)
            {
                
                await HandleExceptionAsync(httpContext, ex);
            }
            _logger.LogDebug("Request pipeline finished.");
        }

        private string CreateMessage(HttpContext context, Exception ex)
        {
            var message = $"Exception caught in global error handler, exception message: {ex.Message}, exception stack: {ex.StackTrace}";

            if (ex.InnerException != null)
            {
                message = $"{message}, inner exception message {ex.InnerException.Message}, inner exception stack {ex.InnerException.StackTrace}";
            }

            return $"{message} RequestId: {context.TraceIdentifier}";
        }

        private Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
        {
            _logger.LogError(CreateMessage(httpContext, exception));
            httpContext.Response.ContentType = "application/json";
       
            return httpContext.Response.WriteAsync(JsonConvert.SerializeObject(
                new ApiError()
                {
                    StatusCode = (int)HttpStatusCode.InternalServerError,
                    Message = "Internal Server Error"
                }));
        }
    }
}
