﻿using Microsoft.EntityFrameworkCore;
using myblog.api.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Infrastructure.Extensions
{
    public static class LinqExtensions
    {
        public static async Task<PagedList<T>> ToPagedList<T>(this IQueryable<T> items, PagingQuery request)
        {
            var result = new PagedList<T>();
            result.PageIndex = request.PageIndex;
            result.PageSize = request.PageSize;
            result.ItemCount = items.Count();
            result.PageCount = (int)Math.Ceiling((double)result.ItemCount / request.PageSize);

            result.Data = await items
                .Take(request.PageSize)
                .Skip(request.PageIndex * request.PageSize).ToListAsync();
                
            return result;
        }
    }
}
