﻿using myblog.api.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Infrastructure.Extensions
{
    public static class ModelConvertExtensions
    {
        public static PagedList<T2> Convert<T1, T2>(this PagedList<T1> source, Func<T1, T2> selector)
        {
            return new PagedList<T2>
            {
                ItemCount = source.ItemCount,
                PageSize = source.PageSize,
                PageIndex = source.PageIndex,
                PageCount = source.PageCount,
                Data = source.Data.Select(selector).ToList()
            };
        }

        public static void NormalizePaging(this PagingQuery request)
        {
            request.PageIndex = request.PageIndex >= 0 ? request.PageIndex : 0;
            request.PageSize = request.PageSize > 0  && request.PageSize < 20 ? request.PageSize : 5;
        }
    }
}
