﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Infrastructure.Helpers
{
    public static class ErrorHandler
    {
        public static void ThrowIf(bool condition, string errorMessage)
        {
            if(condition)
                throw new Exception(errorMessage);
        }
    }
}
