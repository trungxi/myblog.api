﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Infrastructure.Models
{
    public class ApiError
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }
    }
}
