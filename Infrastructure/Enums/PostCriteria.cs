﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Infrastructure.Enums
{
    public enum PostCriteria
    {
        Lastest = 0,
        Popular = 1,
        Trending = 2
    }
}
