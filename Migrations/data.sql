USE [my-blog-db]
GO
SET IDENTITY_INSERT [dbo].[BlogPost] ON 

INSERT [dbo].[BlogPost] ([Id], [Title], [Introduction], [BodyContent], [Thumbnail], [CategoryId], [AuthorName], [CreatedDate], [PublishDate], [Slug]) VALUES (9, N'ASP .NET Core with Docker: Beginers Guide', N'Hi Developer
If you are looking for information about ASP.NET and Docker you came to the right site.
This article will teach you how to create and build an ASP .NET Core application and how to use Docker in your app. Before starting to code let’s talk about these technologies for enter in context.', N'A technical explaination could be a little bit confusing and boring, so I will explain it with our development language to get it quickly
The simple answer is:
ASP.NET is a platform provided by microsoft in the early 2000s that allows developers to create web apps, services, and dynamic websites.
You can use Visual Basic or C# languages to code an ASP.NET application, but the most common practice is code in C#.
What about .NET Core? NET Core is the framework that support ASP.NET, is a cross platform, open source re-implementation of the .NET Framework. If you want to know more in depth, I recommend you this wonderful post about .NET Core.
If you are familiarized with C#, this will be very easy to you to start with ASP.NET, believe me.', N'\Upload\Post\post_2.png', 1, N'Trung xì Nguyễn', CAST(N'2019-11-13T16:00:00.000' AS DateTime), CAST(N'2019-11-13T16:00:00.000' AS DateTime), N'asp-net-core-with-docker-a-beginers-guide-4f490f644a89')
INSERT [dbo].[BlogPost] ([Id], [Title], [Introduction], [BodyContent], [Thumbnail], [CategoryId], [AuthorName], [CreatedDate], [PublishDate], [Slug]) VALUES (13, N'Getting started with ASP .NET Core
', N'ASP .NET Core is an open source framework that allows to build 💪 cloud-based applications such as web servers, IoT and mobile APIs / backbends. In this article, we will create together our first ‘hello world’ web app using the Visual Studio Code IDE.', N'Please note that this framework is different from the ASP .NET framework. If you are looking for ASP .NET, then this article is not for you 😱.
Prerequisites
Download and install the .NET Core SDK :
Download .NET Core
Download the latest releases of .NET Core runtime and tools. Downloads include the .NET Core command line tools, Visual…
www.microsoft.com
We will be using .NET Core 2.0 PREVIEW 2 which is the latest stable version at the time of writing.
Install Visual studio code and these C# extensions:
C# - Visual Studio Marketplace
Extension for Visual Studio Code - C# for Visual Studio Code (powered by OmniSharp).
marketplace.visualstudio.com
Code Runner - Visual Studio Marketplace
Extension for Visual Studio Code - Run C, C++, Java, JS, PHP, Python, Perl, Ruby, Go, Lua, Groovy, PowerShell, CMD…
marketplace.visualstudio.com
C# XML Documentation Comments - Visual Studio Marketplace
Extension for Visual Studio Code - Generate C# XML documentation comments for ///
marketplace.visualstudio.com
C# Extensions - Visual Studio Marketplace
Extension for Visual Studio Code - C# IDE Extensions for VSCode
marketplace.visualstudio.com
🙏 We are ready to create and run a “Hello world” web app.', N'\Upload\Post\post_10.png', 1, N'Trung xì Nguyễn', CAST(N'2019-11-14T18:00:00.000' AS DateTime), CAST(N'2019-11-14T18:00:00.000' AS DateTime), N'getting-started-with-asp-net-core-f0bbbce81ca1')
SET IDENTITY_INSERT [dbo].[BlogPost] OFF
INSERT [dbo].[Category] ([Id], [Name], [Description], [CreatedDate]) VALUES (1, N'Programming', N'Programming', CAST(N'2019-04-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Category] ([Id], [Name], [Description], [CreatedDate]) VALUES (2, N'Life styles', N'Life styles', CAST(N'2019-04-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Category] ([Id], [Name], [Description], [CreatedDate]) VALUES (3, N'Tips & Tricks', N'Tips & Tricks', CAST(N'2019-04-12T00:00:00.000' AS DateTime))
INSERT [dbo].[Category] ([Id], [Name], [Description], [CreatedDate]) VALUES (4, N'Others', N'Others', CAST(N'2019-04-12T00:00:00.000' AS DateTime))
