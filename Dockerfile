FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 5001
EXPOSE 443
ENV ASPNETCORE_URLS=http://+:5001

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY . /src/myblog.api
#COPY ["myblog.api/myblog.api.csproj", "myblog.api/"]
RUN dotnet restore "myblog.api/myblog.api.csproj"
COPY . .
WORKDIR "/src/myblog.api"
RUN dotnet build "myblog.api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "myblog.api.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "myblog.api.dll"]