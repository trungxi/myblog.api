﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myblog.api.Entities;

namespace myblog.api.Dtos
{
    public class PagedList<T>
    {
        public int PageCount { get; set; }
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public int ItemCount { get; set; }
        
        public List<T> Data { get; set; }
    }
}
