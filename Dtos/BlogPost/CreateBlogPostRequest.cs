﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using myblog.api.Entities;

namespace myblog.api.Dtos.BlogPost
{
    public class CreateBlogPostRequest
    {
        public string Title { get; set; }

        public string Slug { get; set; }

        public string Introduction { get; set; }

        public string BodyContent { get; set; }

        public string Thumbnail { get; set; }

        public DateTime PublishDate { get; set; }

        public int CategoryId { get; set; }

        public string AuthorName { get; set; }

        public Entities.BlogPost ToEntity()
        {
            return new Entities.BlogPost
            {
                Title = Title,
                BodyContent = BodyContent,
                Introduction = Introduction,
                PublishDate = PublishDate,
                Slug = Slug,
                Thumbnail = Thumbnail,
                CategoryId = CategoryId,
                AuthorName = AuthorName
            };
        }
    }
}
