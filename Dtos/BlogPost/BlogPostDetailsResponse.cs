﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos.BlogPost
{
    public class BlogPostDetailsResponse : BaseDto
    {
        public string Title { get; set; }

        public string Slug { get; set; }

        public string Introduction { get; set; }

        public string BodyContent { get; set; }

        public DateTime PublishDate { get; set; }

        public string Thumbnail { get; set; }

        public SimpleCategoryResponse Category { get; set; }

        public string AuthorName { get; set; }

        public BlogPostDetailsResponse(Entities.BlogPost entity)
        {
            Id = entity.Id;
            Title = entity.Title;
            Introduction = entity.Introduction;
            Slug = entity.Slug;
            PublishDate = entity.PublishDate;
            AuthorName = entity.AuthorName;
            BodyContent = entity.BodyContent;
            Category = new SimpleCategoryResponse
            {
                Name = entity.Category.Name,
                Id = entity.Category.Id
            };
        }
    }
}
