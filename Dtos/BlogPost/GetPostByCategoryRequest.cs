﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos
{
    public class GetPostByCategoryRequest : PagingQuery
    {
        public int CategoryId { get; set; }
    }
}
