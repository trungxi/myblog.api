﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos.BlogPost
{
    public class UpdateBlogPostRequest: BaseDto
    {
        public string Title { get; set; }

        public string Slug { get; set; }

        public string Introduction { get; set; }

        public string BodyContent { get; set; }

        public string Thumbnail { get; set; }

        public DateTime PublishDate { get; set; }

        public int CategoryId { get; set; }

    }
}
