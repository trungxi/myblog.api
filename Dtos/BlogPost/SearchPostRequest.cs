﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos.BlogPost
{
    public class SearchPostRequest : PagingQuery
    {
        public string Keyword { get; set; }
    }
}
