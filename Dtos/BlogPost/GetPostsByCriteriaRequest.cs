﻿using myblog.api.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos.BlogPost
{
    public class GetPostsByCriteriaRequest : PagingQuery
    {
        public PostCriteria Criteria { get; set; }
    }
}
