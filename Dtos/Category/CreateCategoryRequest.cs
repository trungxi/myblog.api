﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos.Category
{
    public class CreateCategoryRequest
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public Entities.Category ToEntity()
        {
            return new Entities.Category
            {
                Name = Name,
                Description = Description
            };
        }
    }
}
