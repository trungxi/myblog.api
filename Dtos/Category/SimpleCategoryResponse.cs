﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos
{
    public class SimpleCategoryResponse : BaseDto
    {
        public string Name { get; set; }
    }
}
