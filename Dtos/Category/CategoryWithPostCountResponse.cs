﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos
{
    public class CategoryWithPostCountResponse : BaseDto
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int PostCount { get; set; }
    }
}
