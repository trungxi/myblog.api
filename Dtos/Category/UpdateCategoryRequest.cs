﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos.Category
{
    public class UpdateCategoryRequest : BaseDto
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
