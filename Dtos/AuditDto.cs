﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos
{
    public class AuditDto : BaseDto
    {
        public DateTime CreatedDate { get; set; }
    }
}
