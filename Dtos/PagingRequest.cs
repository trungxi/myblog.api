﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Dtos
{
    public class PagingQuery
    {
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
    }
}
