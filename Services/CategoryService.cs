﻿using myblog.api.Dtos;
using myblog.api.Dtos.Category;
using myblog.api.Entities;
using myblog.api.Infrastructure.Helpers;
using myblog.api.Respositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRespository categoryRepository;
        public CategoryService(ICategoryRespository categoryRepository)
        {
            this.categoryRepository = categoryRepository;
        }

        public async Task<IEnumerable<CategoryWithPostCountResponse>> GetAllWithPostCountAsync()
        {
            var categories = await categoryRepository.GetAllWithPostCount();

            return categories.Select(c => new CategoryWithPostCountResponse
            {
                Id = c.Id,
                Name = c.Name,
                PostCount = c.PostCount,
                Description = c.Description
            });
        }

        public async Task<IEnumerable<Category>> GetAllAsync()
        {
            return await categoryRepository.GetAll();
        }

        public async Task<Category> GetByIdAsync(int id)
        {
            return await categoryRepository.GetById(id);
        }

        public async Task CreateAsync(CreateCategoryRequest request)
        {
            var category = request.ToEntity();
            category.CreatedDate = DateTime.Now;
            await categoryRepository.Add(category);
        }

        public async Task UpdateAsync(UpdateCategoryRequest request)
        {
            var category = await categoryRepository.GetById(request.Id);
            ErrorHandler.ThrowIf(category == null, "Category is not exist");
            category.Name = request.Name;
            category.Description = request.Description;
            await categoryRepository.Update(category);
        }

        public async Task DeleteAsync(BaseDto request)
        {
            var category = await categoryRepository.GetById(request.Id);
            ErrorHandler.ThrowIf(category == null, "Category is not exist");

            await categoryRepository.Remove(category);
        }
    }
}
