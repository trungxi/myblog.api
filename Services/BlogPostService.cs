﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using myblog.api.Dtos;
using myblog.api.Dtos.BlogPost;
using myblog.api.Entities;
using myblog.api.Infrastructure.Enums;
using myblog.api.Infrastructure.Extensions;
using myblog.api.Infrastructure.Helpers;
using myblog.api.Respositories;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace myblog.api.Services
{
    public class BlogPostService : IBlogPostService
    {
        readonly IConfiguration configuration;
        private readonly IBlogPostRepository blogPostRepository;
        public BlogPostService(IBlogPostRepository blogPostRepository, IConfiguration configuration)
        {
            this.blogPostRepository = blogPostRepository;
            this.configuration = configuration;
        }

        public async Task<PagedList<SimpleBlogPostResponse>> GetByCategoryAsync(GetPostByCategoryRequest request)
        {
            request.NormalizePaging();
            Expression<Func<BlogPost, bool>> getByCategoryPredicate = (post) => post.CategoryId == request.CategoryId;

            var pagedResults = await blogPostRepository.QueryPagedList(getByCategoryPredicate, request);

            Func<BlogPost, SimpleBlogPostResponse> selector = d => {
                var post = new SimpleBlogPostResponse(d);
                post.Thumbnail = ResolveRelativePath(d.Thumbnail);
                return post;
            };

            return pagedResults.Convert(selector);
        }

        public async Task<BlogPostDetailsResponse> GetByIdAsync(int id)
        {
            var blogPost = await blogPostRepository.GetBlogById(id);

            ErrorHandler.ThrowIf(blogPost == null, "Post is not exist");

            var postResult = new BlogPostDetailsResponse(blogPost);
            postResult.Thumbnail = ResolveRelativePath(blogPost.Thumbnail);
            return postResult;
        }

        public async Task<BlogPostDetailsResponse> GetBySlug(string slug)
        {
            var blogPost = await blogPostRepository.Find(b => b.Slug == slug);

            ErrorHandler.ThrowIf(blogPost == null, "Post is not exist");

            var postResult = new BlogPostDetailsResponse(blogPost);
            postResult.Thumbnail = ResolveRelativePath(blogPost.Thumbnail);
            return postResult;
        }

        public async Task<PagedList<SimpleBlogPostResponse>> GetPostsByCriteriaAsync(GetPostsByCriteriaRequest request)
        {
            request.NormalizePaging();
            PagedList<BlogPost> result = null;

            switch (request.Criteria)
            {
                case PostCriteria.Lastest:
                default:
                    result = await blogPostRepository.Query()
                                    .OrderByDescending(b => b.PublishDate)
                                    .Include(x => x.Category)
                                    .ToPagedList(request);
                    break;
            }
            Func<BlogPost, SimpleBlogPostResponse> selector = d => {
                var post = new SimpleBlogPostResponse(d);
                post.Thumbnail = ResolveRelativePath(d.Thumbnail);
                return post;
            };

            return result.Convert(selector);
        }

        public async Task CreateAsync(CreateBlogPostRequest request)
        {
            var blogPost = request.ToEntity();
            blogPost.CreatedDate = DateTime.Now;
            await blogPostRepository.Add(blogPost);
        }

        public async Task DeleteAsync(BaseDto request)
        {
            var blogPost = await blogPostRepository.GetById(request.Id);
            ErrorHandler.ThrowIf(blogPost == null, "Post is not exist");

            await blogPostRepository.Remove(blogPost);
        }

        public async Task UpdateAsync(UpdateBlogPostRequest request)
        {
            var blogPost = await blogPostRepository.GetById(request.Id);
            ErrorHandler.ThrowIf(blogPost == null, "Post is not exist");

            blogPost.Title = request.Title;
            blogPost.Introduction = request.Introduction;
            blogPost.Slug = request.Slug;
            blogPost.BodyContent = request.BodyContent;
            blogPost.PublishDate = request.PublishDate;
            blogPost.Thumbnail = request.Thumbnail;
            blogPost.CategoryId = request.CategoryId;

            await blogPostRepository.Update(blogPost);
        }

        public async Task<PagedList<SimpleBlogPostResponse>> SearchPostsAsync(SearchPostRequest request)
        {
            request.NormalizePaging();
            Expression<Func<BlogPost, bool>> searchPredicate = (post) => post.Title.Contains(request.Keyword) || post.Introduction.Contains(request.Keyword);
            
            var pagedResults = await blogPostRepository.QueryPagedList(searchPredicate, request);

            Func<BlogPost, SimpleBlogPostResponse> selector = d => {
                var post = new SimpleBlogPostResponse(d);
                post.Thumbnail = ResolveRelativePath(d.Thumbnail);
                return post;
            };

            return pagedResults.Convert(selector);
        }

        public string ResolveRelativePath(string relativePath)
        {
            return configuration["Hosting:Url"] + relativePath;
        }
    }
}
