﻿using myblog.api.Dtos;
using myblog.api.Dtos.Category;
using myblog.api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Services
{
    public interface ICategoryService
    {
        Task<IEnumerable<CategoryWithPostCountResponse>> GetAllWithPostCountAsync();
        Task<IEnumerable<Category>> GetAllAsync();
        Task<Category> GetByIdAsync(int id);
        Task CreateAsync(CreateCategoryRequest request);
        Task UpdateAsync(UpdateCategoryRequest request);
        Task DeleteAsync(BaseDto request);
    }
}
