﻿using myblog.api.Dtos;
using myblog.api.Dtos.BlogPost;
using myblog.api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Services
{
    public interface IBlogPostService
    {
        Task<PagedList<SimpleBlogPostResponse>> GetPostsByCriteriaAsync(GetPostsByCriteriaRequest request);
        Task<PagedList<SimpleBlogPostResponse>> SearchPostsAsync(SearchPostRequest request);
        Task<PagedList<SimpleBlogPostResponse>> GetByCategoryAsync(GetPostByCategoryRequest request);
        Task<BlogPostDetailsResponse> GetByIdAsync(int id);

        Task<BlogPostDetailsResponse> GetBySlug(string slug);

        Task CreateAsync(CreateBlogPostRequest request);
        Task UpdateAsync(UpdateBlogPostRequest request);
        Task DeleteAsync(BaseDto request);
    }
}
