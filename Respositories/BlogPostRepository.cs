﻿using Microsoft.EntityFrameworkCore;
using myblog.api.Dtos;
using myblog.api.Entities;
using myblog.api.Infrastructure.Extensions;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace myblog.api.Respositories
{
    public class BlogPostRepository : EFRepository<BlogPost>, IBlogPostRepository
    {
        public BlogPostRepository(BlogDbContext dbContext) : base(dbContext)
        {

        }

        public async Task<BlogPost> GetBlogById(int id)
        {
            return await dbSet.Include(b => b.Category).FirstOrDefaultAsync(b => b.Id == id);
        }


        public async Task<PagedList<BlogPost>> QueryPagedList(Expression<Func<BlogPost, bool>> predicate, PagingQuery pagingQuery)
        {
            return await GetWhere(predicate).Include(x => x.Category).ToPagedList(pagingQuery);
        }
    }
}
