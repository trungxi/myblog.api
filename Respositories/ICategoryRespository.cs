﻿using myblog.api.Entities;
using myblog.api.Respositories.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Respositories
{
    public interface ICategoryRespository : IAsyncRepository<Category>
    {
        Task<IEnumerable<CategoryQueryModel>> GetAllWithPostCount();
    }
}
