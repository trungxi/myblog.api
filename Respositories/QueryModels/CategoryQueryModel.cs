﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Respositories.QueryModels
{
    public class CategoryQueryModel : BaseQueryModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public int PostCount { get; set; }
    }
}
