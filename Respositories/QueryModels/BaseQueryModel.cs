﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Respositories.QueryModels
{
    public class BaseQueryModel
    {
        public int Id { get; set; }

        public DateTime CreatedDate { get; set; }
    }
}
