﻿using myblog.api.Dtos;
using myblog.api.Dtos.BlogPost;
using myblog.api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace myblog.api.Respositories
{
    public interface IBlogPostRepository: IAsyncRepository<BlogPost>
    {
        Task<BlogPost> GetBlogById(int id);
        Task<PagedList<BlogPost>> QueryPagedList(Expression<Func<BlogPost, bool>> predicate, PagingQuery pagingQuery);
    }
}
