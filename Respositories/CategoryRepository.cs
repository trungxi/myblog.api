﻿using Microsoft.EntityFrameworkCore;
using myblog.api.Entities;
using myblog.api.Respositories.QueryModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace myblog.api.Respositories
{
    public class CategoryRepository : EFRepository<Category>, ICategoryRespository
    {
        public CategoryRepository(BlogDbContext dbContext) : base(dbContext)
        {

        }

        public async Task<IEnumerable<CategoryQueryModel>> GetAllWithPostCount()
        {
            return await this.dbSet.Select(c => new CategoryQueryModel
            {
                Id = c.Id,
                Name = c.Name,
                Description = c.Description,
                PostCount = c.BlogPosts.Count,
                CreatedDate = c.CreatedDate
            }).ToListAsync();
        }
    }
}
