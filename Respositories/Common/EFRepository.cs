﻿using Microsoft.EntityFrameworkCore;
using myblog.api.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace myblog.api.Respositories
{
    public class EFRepository<T> : IAsyncRepository<T> where T : BaseEntity
    {
        #region Fields

        protected BlogDbContext Context;

        protected DbSet<T> dbSet;

        #endregion

        public EFRepository(BlogDbContext context)
        {
            Context = context;
            this.dbSet = context.Set<T>();
        }

        #region Public Methods

        public async Task<bool> Exist(Expression<Func<T, bool>> predicate)
        {
            return await dbSet.AnyAsync(predicate);
        }

        public Task<T> GetById(int id) => Context.Set<T>().FindAsync(id);

        public Task<T> Find(Expression<Func<T, bool>> predicate)
            => Context.Set<T>().FirstOrDefaultAsync(predicate);

        public async Task Add(T entity)
        {
            await Context.Set<T>().AddAsync(entity);
            await Context.SaveChangesAsync();
        }

        public Task Update(T entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
            return Context.SaveChangesAsync();
        }

        public Task Remove(T entity)
        {
            Context.Set<T>().Remove(entity);
            return Context.SaveChangesAsync();
        }

        public Task<List<T>> GetAll()
        {
            return Context.Set<T>().ToListAsync();
        }

        public IQueryable<T> Query()
        {
            return dbSet.AsQueryable();
        }

        public IQueryable<T> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return Context.Set<T>().Where(predicate);
        }

        public Task<int> CountAll() => Context.Set<T>().CountAsync();

        public Task<int> CountWhere(Expression<Func<T, bool>> predicate)
            => Context.Set<T>().CountAsync(predicate);

        #endregion
    }
}

